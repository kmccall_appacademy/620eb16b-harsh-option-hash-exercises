# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```

def transmogrify(word, options={})
  default = { times: 1}
  default = default.merge(options)
  final_string = times(word,default[:times])
  final_string.map! {|word| word.upcase} if default[:upcase] && !default[:upcase].nil?
  final_string.map! {|word| word.reverse} if default[:reverse] && !default[:reverse].nil?
  final_string.join
end

def times(word,num)
  arr = []
  (0...num).each do
    arr << word
  end
  arr
end
